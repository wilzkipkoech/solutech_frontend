# Solutech Vue Applications

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Run your tests
```
yarn run test
```

### Lints and fixes files
```
yarn run lint
```
### Solutions

I was able to host my front end on my serve by dockerizing my app
```
http://196.216.82.145:57/

```

###Test Credentials

The following are log in credentials that you could use to access the system

EMAIL  kosgeiwilson95@gmail.com

PASSWORD     12345678


You can as well register a new user.


### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
