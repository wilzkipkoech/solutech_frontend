export default {
  state: {
    app: {}
  },

  getters: {
    getCurrentApp(state) {
      return state.app
    }
  },

  mutations: {
    setCurrentApp(state, data) {
      state.app = data
    }
  },

  actions: {
    setCurrentApp({ commit }, data) {
      commit('setCurrentApp', data)
    }
  }
}