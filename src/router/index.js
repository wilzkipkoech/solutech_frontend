import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    component: () =>
      import(/* webpackChunkName: "login" */ "../views/Redirect.vue"),
  },
  {
    path: "/login",
    name: "login",
    component: () =>
      import(/* webpackChunkName: "login" */ "../views/auth/Login.vue"),
    meta: {
      guest: true,
    },
  },
  {
    path: "/register",
    name: "register",
    component: () =>
      import(/* webpackChunkName: "register" */ "../views/auth/Register.vue"),
    meta: {
      guest: true,
    },
  },
  {
    path: "/forgot-password",
    name: "forgot-password",
    component: () =>
      import(
        /* webpackChunkName: "forgot-password" */ "../views/auth/ForgotPassword.vue"
      ),
    meta: {
      guest: true,
    },
  },

  {
    path: "/dashboard",
    name: "dashboard",
    component: () =>
      import(/* webpackChunkName: "dashboard" */ "../views/Dashboard.vue"),
    children: [
      {
        path: "",
        redirect: "my-apps",
      },
      // {
      //   path: '',
      //   name: 'dashboard',
      //   component: () => import(/* webpackChunkName: "home" */ '../views/dashboard/Home.vue')
      // },
   

      {
        path: "products",
        name: "products",
        component: () =>
          import(
            /* webpackChunkName: "my-groups" */ "../views/dashboard/products/Index.vue"
          ),
      },
      {
        path: "products-details/:id",
        name: "products-details",
        component: () =>
          import(
            /* webpackChunkName: "my-groups" */ "../views/dashboard/products/View.vue"
          ),
      },
      {
        path: "create-product",
        name: "create-product",
        component: () =>
          import(
            /* webpackChunkName: "my-groups" */ "../views/dashboard/products/Create.vue"
          ),
      },
      {
        path: "products/:id",
        name: "products.show",
        component: () =>
          import(
            /* webpackChunkName: "groups.show" */ "../views/dashboard/products/Show.vue"
          ),
      },
      {
        path: "orders",
        name: "orders",
        component: () =>
          import(
            /* webpackChunkName: "my-groups" */ "../views/dashboard/orders/Index.vue"
          ),
      },
      {
        path: "orders-details/:id",
        name: "orders-details",
        component: () =>
          import(
            /* webpackChunkName: "my-groups" */ "../views/dashboard/orders/View.vue"
          ),
      },
      {
        path: "create-orders",
        name: "create-orders",
        component: () =>
          import(
            /* webpackChunkName: "my-groups" */ "../views/dashboard/orders/Create.vue"
          ),
      },
      {
        path: "orders/:id",
        name: "orders.show",
        component: () =>
          import(
            /* webpackChunkName: "groups.show" */ "../views/dashboard/orders/Show.vue"
          ),
      },
      {
        path: "orders_details",
        name: "orders_details",
        component: () =>
          import(
            /* webpackChunkName: "my-groups" */ "../views/dashboard/orders_details/Index.vue"
          ),
      },
      {
        path: "orders_details-details/:id",
        name: "orders_details-details",
        component: () =>
          import(
            /* webpackChunkName: "my-groups" */ "../views/dashboard/orders_details/View.vue"
          ),
      },
      // {
      //   path: "create-orders_details",
      //   name: "create-orders_details",
      //   component: () =>
      //     import(
      //       /* webpackChunkName: "my-groups" */ "../views/dashboard/orders_details/Create.vue"
      //     ),
      // },
      {
        path: "orders_details/:id",
        name: "orders_details.show",
        component: () =>
          import(
            /* webpackChunkName: "groups.show" */ "../views/dashboard/orders_details/Show.vue"
          ),
      },
      {
        path: "supplier",
        name: "supplier",
        component: () =>
          import(
            /* webpackChunkName: "my-groups" */ "../views/dashboard/suppliers/Index.vue"
          ),
      },
      {
        path: "create-supplier",
        name: "create-supplier",
        component: () =>
          import(
            /* webpackChunkName: "my-groups" */ "../views/dashboard/suppliers/Create.vue"
          ),
      },
      {
        path: "supplier-product",
        name: "supplier-product",
        component: () =>
          import(
            /* webpackChunkName: "my-groups" */ "../views/dashboard/suppliers/Product.vue"
          ),
      },
      {
        path: "product-supplier",
        name: "product-supplier",
        component: () =>
          import(
            /* webpackChunkName: "my-groups" */ "../views/dashboard/suppliers/Supplierproduct.vue"
          ),
      },
      {
        path: "supplier/:id",
        name: "supplier.show",
        component: () =>
          import(
            /* webpackChunkName: "groups.show" */ "../views/dashboard/suppliers/Show.vue"
          ),
      },
      {
        path: "my-apps",
        name: "my-apps",
        component: () =>
          import(
            /* webpackChunkName: "my-apps" */ "../views/dashboard/main/Index.vue"
          ),
      },
    ],
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  scrollBehavior() {
    return { x: 0, y: 0 };
  },
  routes,
});
export default router;
